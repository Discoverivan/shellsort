﻿namespace Shellsort
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.generateArray = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.arraySize = new System.Windows.Forms.TextBox();
            this.stepLabel = new System.Windows.Forms.Label();
            this.stepTextBox = new System.Windows.Forms.TextBox();
            this.nextButton = new System.Windows.Forms.Button();
            this.programSteps = new System.Windows.Forms.Label();
            this.prevButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.stepButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.currentAction = new System.Windows.Forms.Label();
            this.toEndButton = new System.Windows.Forms.Button();
            this.toStartButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // generateArray
            // 
            this.generateArray.Location = new System.Drawing.Point(168, 30);
            this.generateArray.Name = "generateArray";
            this.generateArray.Size = new System.Drawing.Size(96, 23);
            this.generateArray.TabIndex = 0;
            this.generateArray.Text = "Сгенерировать";
            this.generateArray.UseVisualStyleBackColor = true;
            this.generateArray.Click += new System.EventHandler(this.generateArray_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Размер массива:";
            // 
            // arraySize
            // 
            this.arraySize.Location = new System.Drawing.Point(139, 32);
            this.arraySize.MaxLength = 2;
            this.arraySize.Name = "arraySize";
            this.arraySize.Size = new System.Drawing.Size(23, 20);
            this.arraySize.TabIndex = 2;
            this.arraySize.Text = "34";
            // 
            // stepLabel
            // 
            this.stepLabel.AutoSize = true;
            this.stepLabel.Location = new System.Drawing.Point(41, 62);
            this.stepLabel.Name = "stepLabel";
            this.stepLabel.Size = new System.Drawing.Size(92, 13);
            this.stepLabel.TabIndex = 3;
            this.stepLabel.Text = "Шаг сортировки:";
            // 
            // stepTextBox
            // 
            this.stepTextBox.Location = new System.Drawing.Point(139, 59);
            this.stepTextBox.MaxLength = 2;
            this.stepTextBox.Name = "stepTextBox";
            this.stepTextBox.Size = new System.Drawing.Size(23, 20);
            this.stepTextBox.TabIndex = 4;
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(623, 41);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(110, 27);
            this.nextButton.TabIndex = 5;
            this.nextButton.Text = "Вперед";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // programSteps
            // 
            this.programSteps.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.programSteps.Location = new System.Drawing.Point(511, 41);
            this.programSteps.Name = "programSteps";
            this.programSteps.Size = new System.Drawing.Size(106, 27);
            this.programSteps.TabIndex = 6;
            this.programSteps.Text = "###/###";
            this.programSteps.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // prevButton
            // 
            this.prevButton.Enabled = false;
            this.prevButton.Location = new System.Drawing.Point(395, 41);
            this.prevButton.Name = "prevButton";
            this.prevButton.Size = new System.Drawing.Size(110, 27);
            this.prevButton.TabIndex = 7;
            this.prevButton.Text = "Назад";
            this.prevButton.UseVisualStyleBackColor = true;
            this.prevButton.Click += new System.EventHandler(this.prevButton_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.Control;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Location = new System.Drawing.Point(306, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(516, 70);
            this.label2.TabIndex = 8;
            // 
            // stepButton
            // 
            this.stepButton.Location = new System.Drawing.Point(168, 57);
            this.stepButton.Name = "stepButton";
            this.stepButton.Size = new System.Drawing.Size(96, 23);
            this.stepButton.TabIndex = 9;
            this.stepButton.Text = "Применить";
            this.stepButton.UseVisualStyleBackColor = true;
            this.stepButton.Click += new System.EventHandler(this.stepButton_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.Control;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Location = new System.Drawing.Point(25, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(260, 70);
            this.label3.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.Control;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.Location = new System.Drawing.Point(842, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(396, 70);
            this.label4.TabIndex = 11;
            // 
            // currentAction
            // 
            this.currentAction.Location = new System.Drawing.Point(853, 29);
            this.currentAction.Name = "currentAction";
            this.currentAction.Size = new System.Drawing.Size(375, 51);
            this.currentAction.TabIndex = 13;
            this.currentAction.Text = "currentAction";
            this.currentAction.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // toEndButton
            // 
            this.toEndButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toEndButton.Location = new System.Drawing.Point(739, 41);
            this.toEndButton.Name = "toEndButton";
            this.toEndButton.Size = new System.Drawing.Size(65, 27);
            this.toEndButton.TabIndex = 14;
            this.toEndButton.Text = "В конец";
            this.toEndButton.UseVisualStyleBackColor = true;
            this.toEndButton.Click += new System.EventHandler(this.toEndButton_Click);
            // 
            // toStartButton
            // 
            this.toStartButton.Enabled = false;
            this.toStartButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toStartButton.Location = new System.Drawing.Point(324, 41);
            this.toStartButton.Name = "toStartButton";
            this.toStartButton.Size = new System.Drawing.Size(65, 27);
            this.toStartButton.TabIndex = 15;
            this.toStartButton.Text = "В начало";
            this.toStartButton.UseVisualStyleBackColor = true;
            this.toStartButton.Click += new System.EventHandler(this.toStartButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(1099, 189);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(153, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Выполнил: Притыченко И. А.";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 211);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.toStartButton);
            this.Controls.Add(this.toEndButton);
            this.Controls.Add(this.currentAction);
            this.Controls.Add(this.stepButton);
            this.Controls.Add(this.prevButton);
            this.Controls.Add(this.programSteps);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.stepTextBox);
            this.Controls.Add(this.stepLabel);
            this.Controls.Add(this.arraySize);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.generateArray);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(1280, 250);
            this.MinimumSize = new System.Drawing.Size(1280, 250);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Сортировка Шелла";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button generateArray;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox arraySize;
        private System.Windows.Forms.Label stepLabel;
        private System.Windows.Forms.TextBox stepTextBox;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Label programSteps;
        private System.Windows.Forms.Button prevButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button stepButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label currentAction;
        private System.Windows.Forms.Button toEndButton;
        private System.Windows.Forms.Button toStartButton;
        private System.Windows.Forms.Label label5;
    }
}

