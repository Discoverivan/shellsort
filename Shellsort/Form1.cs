﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Shellsort
{
    public partial class Form1 : Form
    {
        const int marginTop=115;
        int width, lastLength, arrayLength,shellStep,sortCurrentStep,sortAllSteps;
        Boolean labels = false,nextPressed;
        Random rand = new Random();
        int[] array = new int[100];
        int[] tmpArray = new int[100];
        int[,] stepsBuffer = new int[4,1000];//два числа которые сравниваются и текущий шаг
        int[,] arrayBuffer = new int[1000, 100]; //Состояния массивов на каждом шаге
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            arrayLength = Convert.ToInt32(arraySize.Text);         
            GenerateArray();
        }

        //КНОПКА ГЕНЕРИРОВАТЬ
        private void generateArray_Click(object sender, EventArgs e)
        {
            string allowSymbols = "0123456789";
            Boolean f = false;
            for (int i = 0; i < (arraySize.Text.Length); i++)
            {
                if (allowSymbols.IndexOf(arraySize.Text[i]) == -1)
                {
                    f = true;
                    break;
                }
            }
            if (f==true)
            {
                MessageBox.Show("Некорректный ввод", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                f = false;
            } else
            if ((Convert.ToInt32(arraySize.Text) < 1))
            {
                MessageBox.Show("Минимальное значение: 1", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                arraySize.Text = "1";
                toStartButton.Enabled = false;
                toEndButton.Enabled = true;
                prevButton.Enabled = false;
                nextButton.Enabled = true;
                arrayLength = Convert.ToInt32(arraySize.Text);
                GenerateArray();
            } else
            if (Convert.ToInt32(arraySize.Text) <= 55)
            {
                toStartButton.Enabled = false;
                toEndButton.Enabled = true;
                prevButton.Enabled = false;
                nextButton.Enabled = true;
                arrayLength = Convert.ToInt32(arraySize.Text);
                GenerateArray();
            } else
            {
                MessageBox.Show("Максимальное значение: 55", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                arraySize.Text = "55";
                toStartButton.Enabled = false;
                toEndButton.Enabled = true;
                prevButton.Enabled = false;
                nextButton.Enabled = true;
                arrayLength = Convert.ToInt32(arraySize.Text);
                GenerateArray();
            }
        }

        //ГЕНЕРАЦИЯ МАССИВА
        void GenerateArray()
        {
            shellStep = arrayLength / 2;
            stepTextBox.Text = "" + shellStep;
            for (int i = 0; i< arrayLength; i++)
            {
                array[i] = rand.Next(1,99);
            }
            uiStart(array);
            shellSort(array);
            actionLabelUpdater("Исходный массив сгенерирован");
        }

        //ЛЕЙБЛЫ
        void uiStart(int[]array)
        {
            if (labels)
            {
                for (int i = 0; i < lastLength; i++)
                {
                    this.Controls.RemoveByKey("arrayElement" + i);
                }
                labels = false;
            }
            if (arrayLength >= 35) { width = 22; } else { width = 35; }
            for (int i = 0; i < arrayLength; i++)
            {
                Label arrayElement = new Label();
                arrayElement.Name = "arrayElement" + i;
                arrayElement.Text = Convert.ToString(array[i]);
                arrayElement.Size = new System.Drawing.Size(width, 35);
                if (i == 0) { arrayElement.Left = 25; }
                else
                { arrayElement.Left = 25 + i * width; }
                arrayElement.Top = marginTop;
                arrayElement.BackColor = Color.White;
                arrayElement.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                arrayElement.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.Controls.Add(arrayElement);
            }
            lastLength = arrayLength;
            labels = true;
        }

        //ПОДСЧЕТ ШАГОВ И ЗАПИСЬ ДЕЙСТВИЙ
        void shellSort(int[] arr)
         {
            for (int h = 0; h < arrayLength; h++)
            {
                arrayBuffer[0, h] = array[h];
                arrayBuffer[1, h] = array[h];
            }
            int j;
            int step = shellStep;
            stepTextBox.Text = "" + step;
            sortAllSteps = 0;
            while (step > 0)
            {
                 for (int i = 0; i < (arrayLength - step); i++)
                 {
                    j = i;
                    sortAllSteps++;
                    stepsBuffer[1, sortAllSteps] = j;
                    stepsBuffer[2, sortAllSteps] = j + step;
                    stepsBuffer[3, sortAllSteps] = step;
                    for (int h = 0; h < arrayLength; h++)
                    {
                        arrayBuffer[sortAllSteps, h] = arr[h];
                    }
                    while ((j >= 0) && (arr[j] > arr[j + step]))
                     {
                        sortAllSteps++;
                        stepsBuffer[1, sortAllSteps] = j;
                        stepsBuffer[2, sortAllSteps] = j + step;
                        stepsBuffer[3, sortAllSteps] = step;
                        int tmp = arr[j];
                        arr[j] = arr[j + step];
                        arr[j + step] = tmp;
                        for (int h = 0; h < arrayLength; h++)
                        {
                            arrayBuffer[sortAllSteps, h] = arr[h];
                        }

                        j -= step;
                     }
                 }
                 step = step / 2;
                 //uiLabels(arr);
             }
            sortAllSteps++;
            for (int h = 0; h < arrayLength; h++)
            {
                arrayBuffer[sortAllSteps, h] = arr[h];
            }
            sortCurrentStep = 0;
             stepsLabelUpdater();
         }

        void actionLabelUpdater(string text)
        {
            currentAction.Text = text;
        }

        void stepsLabelUpdater()
        {
            programSteps.Text = sortCurrentStep + "/" + sortAllSteps;
        }

        //КНОПКА ПРИМЕНИТЬ
        private void stepButton_Click(object sender, EventArgs e)
        {
            string allowSymbols = "0123456789";
            Boolean f = false;
            for (int i = 0; i < (stepTextBox.Text.Length); i++)
            {
                if (allowSymbols.IndexOf(stepTextBox.Text[i]) == -1)
                {
                    f = true;
                    break;
                }
            }
            if (f == true)
            {
                MessageBox.Show("Некорректный ввод", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                f = false;
            }
            else
            if (Convert.ToInt32(stepTextBox.Text) < 1)
            {
                MessageBox.Show("Минимальное значение = 1", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                stepTextBox.Text = "1";
                for (int h = 0; h < arrayLength; h++)
                {
                    array[h] = arrayBuffer[0, h];
                }
                uiStart(array);
                shellStep = Convert.ToInt32(stepTextBox.Text);
                shellSort(array);
                actionLabelUpdater("Исходный массив сгенерирован \n \n Шаг сортировки изменен!");
                toStartButton.Enabled = false;
                toEndButton.Enabled = true;
                prevButton.Enabled = false;
                nextButton.Enabled = true;
            }
            else
            if (Convert.ToInt32(stepTextBox.Text) < arrayLength)
            {
                for (int h = 0; h < arrayLength; h++)
                {
                    array[h] = arrayBuffer[0, h];
                }
                uiStart(array);
                shellStep = Convert.ToInt32(stepTextBox.Text);
                shellSort(array);
                actionLabelUpdater("Исходный массив сгенерирован \n \n Шаг сортировки изменен!");
                toStartButton.Enabled = false;
                toEndButton.Enabled = true;
                prevButton.Enabled = false;
                nextButton.Enabled = true;
            }
            else
            {
                MessageBox.Show("Максимальное значение = " + (arrayLength-1), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                stepTextBox.Text = "" + (arrayLength - 1);
                for (int h = 0; h < arrayLength; h++)
                {
                    array[h] = arrayBuffer[0, h];
                }
                uiStart(array);
                shellStep = Convert.ToInt32(stepTextBox.Text);
                shellSort(array);
                actionLabelUpdater("Исходный массив сгенерирован \n \n Шаг сортировки изменен!");
                toStartButton.Enabled = false;
                toEndButton.Enabled = true;
                prevButton.Enabled = false;
                nextButton.Enabled = true;
            }
        }

        //КНОПКА ВПЕРЕД
        private void nextButton_Click(object sender, EventArgs e)
        {
            prevButton.Enabled = true;
            toStartButton.Enabled = true;
            if (sortCurrentStep < sortAllSteps-1)
            {
                nextStep();
            } else
            if (sortCurrentStep == sortAllSteps-1)
            {
                nextStep();
                nextButton.Enabled = false;
                toEndButton.Enabled = false;
                actionLabelUpdater("Сортировка завершена!");
            }
        }

        //КНОПКА НАЗАД
        private void prevButton_Click(object sender, EventArgs e)
        {
            nextButton.Enabled = true;
            toEndButton.Enabled = true;
            if (sortCurrentStep > 1)
            {
                prevStep();
            }
            else
            if (sortCurrentStep == 1)
            {
                prevStep();
                prevButton.Enabled = false;
                toStartButton.Enabled = false;
                actionLabelUpdater("Исходный массив сгенерирован");
            }
        }

        //КНОПКА В КОНЕЦ
        private void toEndButton_Click(object sender, EventArgs e)
        {
            sortCurrentStep = sortAllSteps;
            stepsLabelUpdater();
            uiStepArray(arrayBuffer, sortCurrentStep);
            prevButton.Enabled = true;
            toStartButton.Enabled = true;
            nextButton.Enabled = false;
            toEndButton.Enabled = false;
            actionLabelUpdater("Сортировка завершена!");
        }

        //КНОПКА В НАЧАЛО
        private void toStartButton_Click(object sender, EventArgs e)
        {
            sortCurrentStep=0;
            stepsLabelUpdater();
            uiStepArray(arrayBuffer, sortCurrentStep);
            prevButton.Enabled = false;
            toStartButton.Enabled = false;
            nextButton.Enabled = true;
            toEndButton.Enabled = true;
            actionLabelUpdater("Исходный массив сгенерирован");
        }

        void nextStep()
        {
            sortCurrentStep++;
            stepsLabelUpdater();
            uiStepArray(arrayBuffer, sortCurrentStep);
            if (arrayBuffer[sortCurrentStep - 1, stepsBuffer[1, sortCurrentStep]] == arrayBuffer[sortCurrentStep, stepsBuffer[2, sortCurrentStep]])
            {
                actionLabelUpdater("Числа " + arrayBuffer[sortCurrentStep, stepsBuffer[2, sortCurrentStep]] + " и " + arrayBuffer[sortCurrentStep, stepsBuffer[1, sortCurrentStep]] + " поменялись местами"+
                    "\n\n Текущее значение шага сортировки: "+ stepsBuffer[3, sortCurrentStep]);
                uiStepColor(stepsBuffer[1, sortCurrentStep], stepsBuffer[2, sortCurrentStep]);
            }
            else
            if (sortCurrentStep < sortAllSteps)
            {
                actionLabelUpdater("Сравниваются числа: " + arrayBuffer[sortCurrentStep, stepsBuffer[1, sortCurrentStep]] + " и " + arrayBuffer[sortCurrentStep, stepsBuffer[2, sortCurrentStep]] +
                    "\n\n Текущее значение шага сортировки: " + stepsBuffer[3, sortCurrentStep]);
                uiStepColor(stepsBuffer[1, sortCurrentStep], stepsBuffer[2, sortCurrentStep]);
            }
        }

        void prevStep()
        {
            sortCurrentStep--;
            stepsLabelUpdater();
            uiStepArray(arrayBuffer, sortCurrentStep);
            if ((sortCurrentStep > 0) && (arrayBuffer[sortCurrentStep - 1, stepsBuffer[1, sortCurrentStep]] == arrayBuffer[sortCurrentStep, stepsBuffer[2, sortCurrentStep]]))
            {
                actionLabelUpdater("Числа " + arrayBuffer[sortCurrentStep, stepsBuffer[2, sortCurrentStep]] + " и " + arrayBuffer[sortCurrentStep, stepsBuffer[1, sortCurrentStep]] + " поменялись местами" +
                    "\n\n Текущее значение шага сортировки: " + stepsBuffer[3, sortCurrentStep]);
                uiStepColor(stepsBuffer[1, sortCurrentStep], stepsBuffer[2, sortCurrentStep]);
            }
            else
            if (sortCurrentStep > 0 && sortCurrentStep < sortAllSteps)
            {
                actionLabelUpdater("Сравниваются числа: " + arrayBuffer[sortCurrentStep, stepsBuffer[1, sortCurrentStep]] + " и " + arrayBuffer[sortCurrentStep, stepsBuffer[2, sortCurrentStep]] +
                    "\n\n Текущее значение шага сортировки: " + stepsBuffer[3, sortCurrentStep]);
                uiStepColor(stepsBuffer[1, sortCurrentStep], stepsBuffer[2, sortCurrentStep]);
            }
        }

        void uiStepArray(int[,] array, int n)
        {
            if (labels)
            {
                for (int i = 0; i < lastLength; i++)
                {
                    this.Controls.RemoveByKey("arrayElement" + i);
                }
                labels = false;
            }
            if (arrayLength >= 35) { width = 22; } else { width = 35; }
            for (int i = 0; i < arrayLength; i++)
            {
                Label arrayElement = new Label();
                arrayElement.Name = "arrayElement" + i;
                arrayElement.Text = Convert.ToString(array[n,i]);
                arrayElement.Size = new System.Drawing.Size(width, 35);
                if (i == 0) { arrayElement.Left = 25; }
                else
                { arrayElement.Left = 25 + i * width; }
                arrayElement.Top = marginTop;
                arrayElement.BackColor = Color.White;
                arrayElement.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                arrayElement.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this.Controls.Add(arrayElement);
            }
            lastLength = arrayLength;
            labels = true;
        }

        void uiStepColor(int n1,int n2)
        {
            Controls["arrayElement" + n1].BackColor = Color.Yellow;
            Controls["arrayElement" + n2].BackColor = Color.Yellow;
        }
    }
}
